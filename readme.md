# RFactor

Shipment price calculator. Now with 100% less reflections. 

For reasoning behind how `Factory` API should work, 
see [Factory/Readme.md](factory-api/readme.md)

## Building

The project uses Maven Wrapper to run Maven's commands. All you really need
is to have Java 8 installed in your PC.

To build the project all you need to do is

```sh
# only on *nix based systems
# windows doesnt seem to care about files
# being executable or not
chmod +x mvnw
./mvnw clean install
```

This invokes compilation on all modules in current project. `runner`
is a special module configured to build binaries, that can be run and
deployed to various targets. Note that the project itself is not
configured to be deployed anywhere, so it's better to compile
on your target before "deploying". 

## Running

Running is as simple as invoking `./app` in `runner/target/appassembler/bin` folder.
Make sure your working directory contains `input.txt` file.

## Testing

Tests are run with JUnit 5.

By default, during every compile related goal, maven runs for all modules
in this project. If such behavior is unnecessary, you may add `-DskipTests`
to skip them. Maven wrapper will make sure all arguments you provide to it
are passed to maven.

Should you want to run tests, you may call `test` goal.


## Requirements for Assignment

Assignment required the following points to be implemented.

* Flexible design to easily add more rules and modify existing ones
  * Done via implementation of `Factory-Api` module in `order-factory`
  * Actual runtime configuration is in `runner` module
* Input is provided in `input.txt` file.
  * It is expected that input is already sorted by date.
  * It is expected that input follows the following format
  ```csv
  date size service
  ``` 
  where: 
    * `date` is `ISO8601` date in format `YYYY-MM-DD`
    * columns are separated by space (`0x20`)
* Implement the following rules:
  * Packages cost the following:
  
  |provider|package size|price|
  |---|---|---|
  |LP|S|1.50 €|
  |LP|M|4.90 €|
  |LP|L|6.90 €|
  |MR|S|2 €|
  |MR|M|3 €|
  |MR|L|4 €|
  
  * Shipments should always match price among providers.
  * All small shipments get a 25% discount
  * Third L LP shipment during one calendar month is free.
  * Accumulated discounts for one calendar month cannot be more than 10 euros
    * All calculations are done in cents. Only during the
    final formatting stage they're converted to floating point numbers
    to prevent issues while dealing with IEEE754 caveats.
    * Sadly I did not provide a simple way to increase resolution
    for all integers. (ex 1 cent being equal to 1000 milicent).
    Due to this, non-standard percentage based discounts
    may cause "rounding" issues while doing integer math.
* Output result to STDOUT in the following format
    ```csv
    date size service "price after discount" "applied discount"
    ```
  * `date` `size` and `service` should match those from line
  they were generated from
  * if it was not possible to generate a result, it should output
  that line and add `Ignored`.
  


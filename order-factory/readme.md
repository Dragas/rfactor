# Order Factory

Implements Factory API for shipment calculator module.

## Notes

Integer based calculation where money are involved are done in cents.

There is no easy way to increase resolution (ex. convert 1 cent to 1000 milicent)

At the time of writing, module's coverage is 67%. To be honest 
I could've configured jacoco for this.

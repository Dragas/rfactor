package lt.saltyjuice.dragas.hw.vinted;

import lt.saltyjuice.dragas.hw.factory.Factory;
import lt.saltyjuice.dragas.hw.factory.Stage;
import lt.saltyjuice.dragas.hw.factory.exception.InconsumableException;
import lt.saltyjuice.dragas.hw.vinted.invoice.Invoice;
import lt.saltyjuice.dragas.hw.vinted.invoice.ShipmentServiceFactory;
import lt.saltyjuice.dragas.hw.vinted.invoice.ShipmentSizeRule;
import lt.saltyjuice.dragas.hw.vinted.order.Order;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ShipmentServiceFactoryTest {

    private static final String SERVICE = "AMAZON";
    private static final String SIZE = "tiny";
    private static final int PRICE = 340;
    private static Factory invoiceFactory;

    @BeforeAll
    public static void initializeFactory() {
        invoiceFactory = new ShipmentServiceFactory(SERVICE);
        invoiceFactory.addRule(new ShipmentSizeRule(SIZE, PRICE));
    }

    @Test
    public void selectsStageAndConsumesInput() throws InconsumableException {
        Order order = new Order();
        order.setShipper(SERVICE);
        order.setSize(SIZE);
        Stage stage = invoiceFactory.getStage(order);
        //i mean all it has to do is not throw
        assertEquals(invoiceFactory, stage);
        Invoice invoice = (Invoice) stage.apply(order);
        assertEquals(order.getShipper(), invoice.getShipper());
        assertEquals(order.getSize(), invoice.getSize());
    }

    @Test
    public void failsToSelectStageWhenShippingServiceDoesntMatch() {
        assertThrows(InconsumableException.class, new Executable() {
            @Override
            public void execute() throws Throwable {
                Order order = new Order();
                order.setSize(SIZE);
                Invoice stage = (Invoice) invoiceFactory.getStage(order).apply(order);
            }
        });
    }

    @Test
    public void failsToSelectStageWhenShipmentSizeDoesntMatch() {
        assertThrows(InconsumableException.class, new Executable() {
            @Override
            public void execute() throws Throwable {
                Order order = new Order();
                order.setShipper(SERVICE);
                Invoice stage = (Invoice) invoiceFactory.getStage(order).apply(order);
            }
        });
    }

}

package lt.saltyjuice.dragas.hw.vinted.discount.confirming;

import lt.saltyjuice.dragas.hw.factory.Rule;
import lt.saltyjuice.dragas.hw.factory.Stage;
import lt.saltyjuice.dragas.hw.factory.exception.InconsumableException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class MonthlyDiscountConfirmingRuleTest {

    private static final int LIMIT = 0;
    private static Rule rule;

    @BeforeAll
    public static void beforeClass() {
        rule = new MonthlyDiscountConfirmingRule(LIMIT);
    }

    @Test
    public void returnsStage() throws InconsumableException {
        Stage stage = rule.getStage(null);
        assertNotNull(stage);
    }

}

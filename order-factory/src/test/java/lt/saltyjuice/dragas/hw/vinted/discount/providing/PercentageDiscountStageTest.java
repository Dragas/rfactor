package lt.saltyjuice.dragas.hw.vinted.discount.providing;

import lt.saltyjuice.dragas.hw.factory.Stage;
import lt.saltyjuice.dragas.hw.factory.exception.InconsumableException;
import lt.saltyjuice.dragas.hw.vinted.invoice.Invoice;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PercentageDiscountStageTest {

    @Test
    public void itAppliesDiscount() throws InconsumableException {
        Stage stage = new PercentageDiscountStage(100);
        Invoice invoice = new Invoice();
        invoice.setPrice(1000);
        ProposedDiscountInvoice proposedInvoice = (ProposedDiscountInvoice) stage.apply(invoice);
        assertEquals(invoice.getPrice(), proposedInvoice.getPrice());
        assertEquals(invoice.getPrice(), proposedInvoice.getDiscount());
    }
}

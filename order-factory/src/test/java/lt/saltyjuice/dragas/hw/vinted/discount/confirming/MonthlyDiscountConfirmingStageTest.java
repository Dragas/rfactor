package lt.saltyjuice.dragas.hw.vinted.discount.confirming;

import lt.saltyjuice.dragas.hw.factory.Stage;
import lt.saltyjuice.dragas.hw.factory.exception.InconsumableException;
import lt.saltyjuice.dragas.hw.vinted.discount.providing.ProposedDiscountInvoice;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class MonthlyDiscountConfirmingStageTest {

    private static final int LIMIT = 900;
    private Stage stage;

    @BeforeEach
    public void setUp() {
        stage = new MonthlyDiscountConfirmingStage(LIMIT);
    }


    @Test
    public void canConsumeEntireLimitInOneGo() throws InconsumableException {
        ProposedDiscountInvoice it = getInvoice(LIMIT, LIMIT);
        ProposedDiscountInvoice result = (ProposedDiscountInvoice) stage.apply(it);
        assertEquals(it, result, "Should return same object");
        assertEquals(LIMIT, it.getDiscount());
        assertEquals(0, it.getPrice());
    }

    @Test
    public void itReducesDiscountToPrice() throws InconsumableException {
        int EXPECTED_PRICE = LIMIT - 1;
        int EXPECTED_DISCOUNT = LIMIT;
        ProposedDiscountInvoice it = getInvoice(EXPECTED_PRICE, EXPECTED_DISCOUNT);
        stage.apply(it);
        assertEquals(EXPECTED_PRICE, it.getDiscount());
    }

    private ProposedDiscountInvoice getInvoice(int price, int discount) {
        ProposedDiscountInvoice it = new ProposedDiscountInvoice();
        it.setDate("7777-77-77");
        it.setPrice(price);
        it.setDiscount(discount);
        return it;
    }

    @Test
    public void itPartiallyReducesDiscount() throws InconsumableException {
        ProposedDiscountInvoice it = getInvoice(LIMIT * 2, LIMIT * 2);
        stage.apply(it);
        assertEquals(LIMIT, it.getDiscount());
        assertEquals(LIMIT, it.getPrice());
    }
}

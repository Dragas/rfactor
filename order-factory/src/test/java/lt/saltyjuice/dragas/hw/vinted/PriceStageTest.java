package lt.saltyjuice.dragas.hw.vinted;

import lt.saltyjuice.dragas.hw.vinted.invoice.Invoice;
import lt.saltyjuice.dragas.hw.vinted.invoice.PriceStage;
import lt.saltyjuice.dragas.hw.vinted.order.Order;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


class PriceStageTest {
    private static final int COST = 350;
    private static final String DATE = "2016-05-01";
    private static final String SHIPPER = "LP";
    private static final String SIZE = "L";

    @Test
    public void generatesInvoice() {
        PriceStage stage = new PriceStage(COST);
        Order order = new Order();
        order.setDate(DATE);
        order.setShipper(SHIPPER);
        order.setSize(SIZE);
        Invoice invoice = stage.apply(order);
        Assertions.assertEquals(order.getDate(), invoice.getDate());
        Assertions.assertEquals(order.getSize(), invoice.getSize());
        Assertions.assertEquals(order.getShipper(), invoice.getShipper());
        Assertions.assertEquals(COST, invoice.getPrice());
    }
}

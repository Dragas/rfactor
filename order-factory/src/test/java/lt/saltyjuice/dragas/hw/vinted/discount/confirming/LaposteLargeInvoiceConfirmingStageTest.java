package lt.saltyjuice.dragas.hw.vinted.discount.confirming;

import lt.saltyjuice.dragas.hw.factory.Stage;
import lt.saltyjuice.dragas.hw.factory.exception.InconsumableException;
import lt.saltyjuice.dragas.hw.vinted.discount.providing.ProposedDiscountInvoice;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class LaposteLargeInvoiceConfirmingStageTest {

    private static final int NTH = 1;
    private static final int DISCOUNT = 1;
    private Stage stage;

    @BeforeEach
    public void setUp() {
        // since this stage mutates itself
        // it needs to be recreated before every test
        stage = new LaposteLargeInvoiceConfirmingStage(NTH);
    }

    @Test
    public void appliesDiscountForNthInvoice() throws InconsumableException {
        ProposedDiscountInvoice invoice = new ProposedDiscountInvoice();
        invoice.setDiscount(DISCOUNT);
        invoice.setDate("77777777777777");
        stage.apply(invoice);
        assertEquals(DISCOUNT, invoice.getDiscount());
    }

    @Test
    public void doesNotApplyDiscountForNonNthInvoice() throws InconsumableException {
        ProposedDiscountInvoice invoice = new ProposedDiscountInvoice();
        invoice.setDiscount(DISCOUNT);
        invoice.setDate("77777777777777");
        for (int i = 0; i <= NTH; i++)
            stage.apply(invoice);
        assertNotEquals(DISCOUNT, invoice.getDiscount());
    }
}

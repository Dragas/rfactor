package lt.saltyjuice.dragas.hw.vinted;

import lt.saltyjuice.dragas.hw.factory.exception.InconsumableException;
import lt.saltyjuice.dragas.hw.vinted.invoice.Invoice;
import lt.saltyjuice.dragas.hw.vinted.invoice.ShipmentSizeRule;
import lt.saltyjuice.dragas.hw.vinted.order.Order;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class ShipmentSizeRuleTest {

    private static final int PRICE = 350;
    private static final String SIZE = "L";
    private static ShipmentSizeRule rule;

    @BeforeAll
    public static void beforeClass() {
        rule = new ShipmentSizeRule(SIZE, PRICE);
    }

    private static Order createOrder(String size) {
        Order order = new Order();
        order.setDate("now");
        order.setSize(size);
        order.setShipper("now");
        return order;
    }

    @Test
    public void generatesInvoiceWhenSizeMatches() throws InconsumableException {
        Order order = createOrder(SIZE);
        Invoice invoice = (Invoice) rule.getStage(order).apply(order);
        Assertions.assertEquals(order.getDate(), invoice.getDate());
        Assertions.assertEquals(order.getSize(), invoice.getSize());
        Assertions.assertEquals(order.getShipper(), invoice.getShipper());
        Assertions.assertEquals(PRICE, invoice.getPrice());
    }

    @Test
    public void failsToGenerateInvoiceWhenSizeDoesntMatch() {
        Assertions.assertThrows(InconsumableException.class, () -> {
            Order order = createOrder("M");
            rule.getStage(order);
        });
    }
}

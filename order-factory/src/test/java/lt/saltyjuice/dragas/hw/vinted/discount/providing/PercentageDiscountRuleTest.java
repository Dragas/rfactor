package lt.saltyjuice.dragas.hw.vinted.discount.providing;

import lt.saltyjuice.dragas.hw.factory.Stage;
import lt.saltyjuice.dragas.hw.factory.exception.InconsumableException;
import lt.saltyjuice.dragas.hw.vinted.invoice.Invoice;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PercentageDiscountRuleTest {

    private PercentageDiscountRule rule;

    @BeforeEach
    public void setUp() {
        rule = new PercentageDiscountRule(100);
    }

    @Test
    public void itReturnsStage() throws InconsumableException {
        Stage stage = rule.getStage(null);// thats undefined behavior right there
        assertNotNull(stage);
    }

    @Test
    public void itFailsToReturnStageForUnmatchingShipper() {
        rule.expectedService("FF");
        Invoice it = new Invoice();
        assertThrows(InconsumableException.class, () -> {
            rule.getStage(it);
        });
    }

    @Test
    public void itFailsToReturnStageForUnmatchingSize() {
        rule.expectedSize("FF");
        Invoice it = new Invoice();
        assertThrows(InconsumableException.class, () -> {
            rule.getStage(it);
        });
    }

    @Test
    public void itReturnsStageForMatchingShipper() {
        rule.expectedService("FF");
        Invoice it = new Invoice();
        it.setShipper("FF");
        assertDoesNotThrow(() -> rule.getStage(it));
    }

    @Test
    public void itReturnsStageForMatchingSize() {
        rule.expectedSize("FF");
        Invoice it = new Invoice();
        it.setSize("FF");
        assertDoesNotThrow(() -> rule.getStage(it));
    }

    @Test
    public void itReturnsStageForMatchingShipperAndSize() {
        rule.expectedSize("FF");
        rule.expectedService("FF");
        Invoice it = new Invoice();
        it.setSize("FF");
        it.setShipper("FF");
        assertDoesNotThrow(() -> rule.getStage(it));
    }
}

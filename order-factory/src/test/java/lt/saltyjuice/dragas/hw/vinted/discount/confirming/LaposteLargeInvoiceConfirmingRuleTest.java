package lt.saltyjuice.dragas.hw.vinted.discount.confirming;

import lt.saltyjuice.dragas.hw.factory.Rule;
import lt.saltyjuice.dragas.hw.factory.Stage;
import lt.saltyjuice.dragas.hw.factory.exception.InconsumableException;
import lt.saltyjuice.dragas.hw.vinted.discount.providing.ProposedDiscountInvoice;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class LaposteLargeInvoiceConfirmingRuleTest {

    private static final String LA_POSTE = "LL";
    private static final String LARGE = "L1";
    private static final int NTH = 0; // doesnt matter in this test
    private static Rule rule;

    @BeforeAll
    public static void beforeClass() {
        rule = new LaposteLargeInvoiceConfirmingRule(LA_POSTE, LARGE, NTH);
    }


    @Test
    public void returnsStageForIntendedInvoice() throws InconsumableException {
        ProposedDiscountInvoice it = new ProposedDiscountInvoice();
        it.setShipper(LA_POSTE);
        it.setSize(LARGE);
        Stage stage = rule.getStage(it);
        assertNotNull(stage);
    }

    @Test
    public void doesNotReturnStageForUnintendedInvoices() throws InconsumableException {
        assertThrows(InconsumableException.class, () -> {
            ProposedDiscountInvoice it = new ProposedDiscountInvoice();
            it.setSize(LARGE);
            Stage stage = rule.getStage(it);
            //assertNull(stage);
        });
        assertThrows(InconsumableException.class, () -> {
            ProposedDiscountInvoice it = new ProposedDiscountInvoice();
            it.setShipper(LA_POSTE);
            Stage stage = rule.getStage(it);
            //assertNull(stage);
        });
        assertThrows(InconsumableException.class, () -> {
            ProposedDiscountInvoice it = new ProposedDiscountInvoice();
            Stage stage = rule.getStage(it);
            //assertNull(stage);
        });
    }
}

package lt.saltyjuice.dragas.hw.vinted.validation;

import lt.saltyjuice.dragas.hw.factory.exception.InconsumableException;

import java.util.Arrays;
import java.util.List;

/**
 * Checks whether or not provided string is contained
 * within prerequired array. Implementations
 * should call super.validate with argument
 * that they're checking if it's valid
 * <p>
 * I guess this works like factories?
 */
public class OneOfValidatingRule extends ValidatingRule {

    private final List<Object> expected;

    public OneOfValidatingRule(Object... expected) {
        this.expected = Arrays.asList(expected);
    }

    @Override
    protected void validate(Object input) throws InconsumableException {
        if (!expected.contains(input))
            throw new InconsumableException(String.format("Expected one of %s, got %s",
                    Arrays.toString(expected.toArray()),
                    input
            ));
    }
}

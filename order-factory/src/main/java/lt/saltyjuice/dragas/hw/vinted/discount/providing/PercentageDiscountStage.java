package lt.saltyjuice.dragas.hw.vinted.discount.providing;

public class PercentageDiscountStage extends ProposedDiscountStage {

    private final int discount;

    public PercentageDiscountStage(int discount) {
        this.discount = discount;
    }

    @Override
    protected void applyDiscount(ProposedDiscountInvoice invoice) {
        int price = invoice.getPrice();
        int discountedPrice = price * discount / 100;
        invoice.setDiscount(discountedPrice);
    }
}

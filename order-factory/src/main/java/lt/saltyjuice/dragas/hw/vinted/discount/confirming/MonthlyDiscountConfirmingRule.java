package lt.saltyjuice.dragas.hw.vinted.discount.confirming;

import lt.saltyjuice.dragas.hw.factory.Stage;
import lt.saltyjuice.dragas.hw.factory.exception.InconsumableException;
import lt.saltyjuice.dragas.hw.vinted.discount.DiscountRule;

/**
 * Discount confirming rule.
 * <p>
 * One of the rules is the monthly limit rule, which
 * should be applied to all incoming invoices.
 *
 * @see MonthlyDiscountConfirmingStage
 */
public class MonthlyDiscountConfirmingRule extends DiscountRule {

    private final int limit;
    private Stage stage;

    public MonthlyDiscountConfirmingRule(int limit) {
        this.limit = limit;
    }

    @Override
    public Stage getStage(Object input) throws InconsumableException {
        if (this.stage == null)
            this.stage = createStage();
        return this.stage;
    }

    @Override
    protected Stage createStage() {
        return new MonthlyDiscountConfirmingStage(limit);
    }
}

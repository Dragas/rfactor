package lt.saltyjuice.dragas.hw.vinted.order;

/**
 * Defines order class used by this application. This
 * particular implementation assumes the following:
 * * All three fields are populated.
 * * Date String field is in ISO8601 format
 */
public class Order {
    private String date = "";
    private String size = "";
    private String shipper = "";

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getShipper() {
        return shipper;
    }

    public void setShipper(String shipper) {
        this.shipper = shipper;
    }
}

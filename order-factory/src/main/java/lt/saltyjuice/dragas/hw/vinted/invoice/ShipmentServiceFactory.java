package lt.saltyjuice.dragas.hw.vinted.invoice;

import lt.saltyjuice.dragas.hw.factory.FirstMatchFactory;
import lt.saltyjuice.dragas.hw.factory.exception.InconsumableException;
import lt.saltyjuice.dragas.hw.vinted.order.Order;

/**
 * An implementation which holds rules for shipment services. When building
 * this object, you should provide rules for sizes, since those
 * are applied depending on shipment service (first matches).
 */
public class ShipmentServiceFactory extends FirstMatchFactory {

    private final String serviceName;

    /**
     * @param serviceName Service name that this factory checks for
     */
    public ShipmentServiceFactory(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceName() {
        return serviceName;
    }

    @Override
    public Object apply(Object element) throws InconsumableException {
        Order input = (Order) element;
        if (!input.getShipper().equals(this.serviceName)) {
            throw new InconsumableException(
                    String.format("Unsupported shipping service. Expected %s, got %s", serviceName, input.getShipper())
            );
        }
        try {
            return super.apply(element);
        } catch (InconsumableException e) {
            throw new InconsumableException(String.format("Unsupported size %s for service %s", input.getSize(), serviceName), e);
        }
        //return (Invoice) stage.apply(input);
    }
}

package lt.saltyjuice.dragas.hw.vinted.discount.providing;

/**
 * Proposed Discount invoices contain all the fields
 * provided in regular invoices, in addition to discount field,
 * which contains a discount that's subject to change in later
 * iterations when manipulating this object. Once the manipulations
 * are done, this object should be converted to {@link lt.saltyjuice.dragas.hw.vinted.discount.DiscountedInvoice}
 * which contains the same fields, except they're immutable.
 */
public class ProposedDiscountInvoice {
    private String date = "";
    private String size = "";
    private String shipper = "";
    private int price;
    private int discount;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getShipper() {
        return shipper;
    }

    public void setShipper(String shipper) {
        this.shipper = shipper;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }
}

package lt.saltyjuice.dragas.hw.vinted.discount.providing;

import lt.saltyjuice.dragas.hw.factory.Stage;
import lt.saltyjuice.dragas.hw.factory.exception.InconsumableException;
import lt.saltyjuice.dragas.hw.vinted.discount.DiscountRule;
import lt.saltyjuice.dragas.hw.vinted.invoice.Invoice;

/**
 * Applies a percentage based discount proposal
 */
public class PercentageDiscountRule extends DiscountRule {

    private final int discount;
    private String service = "";
    private String size = "";
    private Stage stage;

    public PercentageDiscountRule(int discount) {
        this.discount = discount;
    }

    @Override
    public Stage getStage(Object input) throws InconsumableException {
        Invoice invoice = (Invoice) input;
        assertServiceMatches(invoice);
        assertSizeMatches(invoice);
        if (stage == null)
            stage = createStage();
        return stage;
    }

    private void assertServiceMatches(Invoice invoice) throws InconsumableException {
        if (!service.isEmpty() && !invoice.getShipper().equals(service))
            throw new InconsumableException(String.format("Expected shipping service %s, got %s", service, invoice.getShipper()));
    }

    private void assertSizeMatches(Invoice invoice) throws InconsumableException {
        if (!size.isEmpty() && !invoice.getSize().equals(size))
            throw new InconsumableException(String.format("Expected size %s, got %s", size, invoice.getSize()));
    }

    @Override
    protected Stage createStage() {
        return new PercentageDiscountStage(discount);
    }

    /**
     * Sets this rule to expect service property
     *
     * @param service expected service. If the string is not empty, the check is applied
     * @return this
     */
    public PercentageDiscountRule expectedService(String service) {
        if (service != null)
            this.service = service;
        return this;
    }

    /**
     * Sets this rule to expect size property
     *
     * @param size expected size. If the string is not empty, the check is applied
     * @return this
     */
    public PercentageDiscountRule expectedSize(String size) {
        if (size != null)
            this.size = size;
        return this;
    }
}

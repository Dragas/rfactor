package lt.saltyjuice.dragas.hw.vinted.order;

import lt.saltyjuice.dragas.hw.factory.Stage;
import lt.saltyjuice.dragas.hw.vinted.invoice.Invoice;

/**
 * Helper class that casts input objects to Order
 */
public abstract class OrderInvoiceStage implements Stage {
    @Override
    public Object apply(Object input) {
        return apply((Order) input);
    }

    public abstract Invoice apply(Order input);
}

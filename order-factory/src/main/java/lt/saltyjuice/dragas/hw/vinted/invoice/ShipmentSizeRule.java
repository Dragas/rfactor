package lt.saltyjuice.dragas.hw.vinted.invoice;

import lt.saltyjuice.dragas.hw.factory.Rule;
import lt.saltyjuice.dragas.hw.factory.Stage;
import lt.saltyjuice.dragas.hw.factory.exception.InconsumableException;
import lt.saltyjuice.dragas.hw.vinted.order.Order;

/**
 * Basis for shipment size rules.
 */
public class ShipmentSizeRule implements Rule {

    private final String size;
    private final int price;
    private Stage stage;

    public ShipmentSizeRule(String size, int price) {
        this.size = size;
        this.price = price;
    }

    @Override
    public Stage getStage(Object input) throws InconsumableException {
        return getStage((Order) input);
    }

    public Stage getStage(Order input) throws InconsumableException {
        if (!input.getSize().equals(size)) {
            throw new InconsumableException(
                    String.format("Expected size %s, got %s", size, input.getSize())
            );
        }
        if (stage == null)
            stage = createStage(price); // cant call this in constructor unless its final
        return stage;                   // initially thought i would override this with different
    }                                   // stages but i guess not

    public int getPrice() {
        return price;
    }

    public String getSize() {
        return size;
    }

    protected Stage createStage(int price) {
        return new PriceStage(price);
    }
}

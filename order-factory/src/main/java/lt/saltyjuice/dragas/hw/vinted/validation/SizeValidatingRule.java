package lt.saltyjuice.dragas.hw.vinted.validation;

import lt.saltyjuice.dragas.hw.factory.exception.InconsumableException;
import lt.saltyjuice.dragas.hw.vinted.order.Order;

public class SizeValidatingRule extends OneOfValidatingRule {
    public SizeValidatingRule(String... sizes) {
        super((Object[]) sizes);
    }

    @Override
    protected void validate(Object input) throws InconsumableException {
        super.validate(((Order) input).getSize());
    }
}

package lt.saltyjuice.dragas.hw.vinted.invoice;

import lt.saltyjuice.dragas.hw.vinted.order.Order;
import lt.saltyjuice.dragas.hw.vinted.order.OrderInvoiceStage;


/**
 * Creates an invoice from particular order object, retaining its core properties
 */
public class PriceStage extends OrderInvoiceStage {

    private final int price;

    public PriceStage(int price) {
        this.price = price;
    }

    @Override
    public Invoice apply(Order input) {
        Invoice invoice = new Invoice();
        invoice.setDate(input.getDate());
        invoice.setSize(input.getSize());
        invoice.setShipper(input.getShipper());
        invoice.setPrice(price);
        return invoice;
    }
}

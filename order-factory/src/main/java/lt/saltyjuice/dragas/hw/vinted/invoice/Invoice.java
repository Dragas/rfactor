package lt.saltyjuice.dragas.hw.vinted.invoice;

/**
 * Invoices are objects that retain information about their
 * orders, but also include the cost.
 */
public class Invoice {
    private String date = "";
    private String size = "";
    private String shipper = "";
    private int price;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getShipper() {
        return shipper;
    }

    public void setShipper(String shipper) {
        this.shipper = shipper;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
//
//    @Override
//    public String toString() {
//        return String.format("%s %s %s %s", date, shipper, size, price);
//    }
}

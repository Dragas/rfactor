package lt.saltyjuice.dragas.hw.vinted.discount.providing;

import lt.saltyjuice.dragas.hw.factory.Stage;
import lt.saltyjuice.dragas.hw.factory.exception.InconsumableException;
import lt.saltyjuice.dragas.hw.vinted.invoice.Invoice;

public abstract class ProposedDiscountStage implements Stage {

    @Override
    public Object apply(Object input) throws InconsumableException {
        Invoice invoice = (Invoice) input;
        ProposedDiscountInvoice proposed = new ProposedDiscountInvoice();
        proposed.setDate(invoice.getDate());
        proposed.setShipper(invoice.getShipper());
        proposed.setPrice(invoice.getPrice());
        proposed.setSize(invoice.getSize());
        applyDiscount(proposed);
        return proposed;
    }

    protected abstract void applyDiscount(ProposedDiscountInvoice invoice);
}

package lt.saltyjuice.dragas.hw.vinted.discount;

import lt.saltyjuice.dragas.hw.vinted.discount.providing.ProposedDiscountInvoice;

public class DiscountedInvoice {

    private final String date;
    private final String size;
    private final String shipper;
    private final int price;
    private final int discount;

    public DiscountedInvoice(ProposedDiscountInvoice invoice) {
        date = invoice.getDate();
        size = invoice.getSize();
        shipper = invoice.getShipper();
        price = invoice.getPrice();
        discount = invoice.getDiscount();
    }

    public String getDate() {
        return date;
    }

    public String getSize() {
        return size;
    }

    public String getShipper() {
        return shipper;
    }

    public int getPrice() {
        return price;
    }

    public int getDiscount() {
        return discount;
    }

}

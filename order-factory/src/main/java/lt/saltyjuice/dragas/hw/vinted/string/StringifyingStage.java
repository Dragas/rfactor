package lt.saltyjuice.dragas.hw.vinted.string;

import lt.saltyjuice.dragas.hw.factory.Stage;
import lt.saltyjuice.dragas.hw.factory.exception.InconsumableException;
import lt.saltyjuice.dragas.hw.vinted.discount.DiscountedInvoice;

import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;

public class StringifyingStage implements Stage {
    private static final DecimalFormat NUMBER_FORMAT = new DecimalFormat("0.00");

    @Override
    public Object apply(Object input) throws InconsumableException {
        DiscountedInvoice invoice = (DiscountedInvoice) input;
        //StringBuilder sb = new StringBuilder();
        List<String> columns = new LinkedList<>();
        columns.add(invoice.getDate());
        columns.add(invoice.getSize());
        columns.add(invoice.getShipper());
        columns.add(
                NUMBER_FORMAT.format(invoice.getPrice() / 100f)
        );
        columns.add(
                formatNumber(invoice.getDiscount() / 100f)
        );

        return String.join(" ", columns);
    }

    private String formatNumber(double number) {
        if (number != 0)
            return NUMBER_FORMAT.format(number);
        return "-";
    }
}

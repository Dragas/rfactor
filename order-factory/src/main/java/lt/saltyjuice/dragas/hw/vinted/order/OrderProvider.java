package lt.saltyjuice.dragas.hw.vinted.order;

import java.io.InputStream;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Scanner;
import java.util.function.Consumer;

/**
 * Returns {@link Order} objects. Since a requirement
 * is that Orders are obtained from an input.txt file,
 * this particular class implements that.
 * <p>
 * Should there be a need to refactor it into an abstract {@link Order} provider,
 * this could be easily by creating such class, implementing the {@link Iterator<Order>}
 * interface and then having this class implement that abstract class.
 * <p>
 * By default it already provides support for {@link InputStream} abstract class.
 *
 * @see OrderProvider#next()
 */
public class OrderProvider implements Iterator<Order> {

    /**
     * sin stands for Scanner INput. Wraps the provided
     * InputStream implementation to provide simple
     * access to line by line based access.
     */
    private final Scanner sin;

    public OrderProvider() {
        this(System.in);
    }

    public OrderProvider(InputStream in) {
        this.sin = new Scanner(in);
    }

    /**
     * By default this does nothing as this class only wraps
     * provided input stream with scanner.
     */
    @Override
    public void remove() {
        // Should it skip a line when
        // this method is invoked?
        //next()
    }

    @Override
    public void forEachRemaining(Consumer<? super Order> action) {
        while (hasNext()) {
            Order it = next();
            action.accept(it);
        }
    }

    @Override
    public boolean hasNext() {
        return sin.hasNextLine();
    }

    /**
     * Assumes that each line has 3 columns separated with spaces:
     * * The first column is ISO8601 date.
     * * The second column is size of shipment.
     * * The third column is mailing service's acronym
     * <p>
     * It is up to consumers to decide whether or not provided
     * objects are considered "valid".
     *
     * @return Order object from wrapped input.
     */
    @Override
    public Order next() {
        String definition = sin.nextLine();
        String[] columns = definition.split(" ");
        int originalLength = columns.length;
        // pads the array with null objects or only takes first 3 items
        columns = Arrays.copyOfRange(columns, 0, 3, String[].class);
        if (originalLength < 3)
            Arrays.fill(columns, originalLength, columns.length, "");
        Order order = new Order();
        order.setDate(columns[0]);
        order.setSize(columns[1]);
        order.setShipper(columns[2]);
        return order;
    }
}

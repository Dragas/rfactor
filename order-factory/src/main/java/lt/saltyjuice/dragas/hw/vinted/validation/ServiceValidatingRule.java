package lt.saltyjuice.dragas.hw.vinted.validation;

import lt.saltyjuice.dragas.hw.factory.exception.InconsumableException;
import lt.saltyjuice.dragas.hw.vinted.order.Order;

public class ServiceValidatingRule extends OneOfValidatingRule {
    public ServiceValidatingRule(String... services) {
        super((Object[]) services);
    }

    @Override
    protected void validate(Object input) throws InconsumableException {
        super.validate(((Order) input).getShipper());
    }
}

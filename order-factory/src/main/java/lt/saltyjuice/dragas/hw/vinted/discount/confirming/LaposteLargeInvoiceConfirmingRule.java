package lt.saltyjuice.dragas.hw.vinted.discount.confirming;

import lt.saltyjuice.dragas.hw.factory.Stage;
import lt.saltyjuice.dragas.hw.factory.exception.InconsumableException;
import lt.saltyjuice.dragas.hw.vinted.discount.DiscountRule;
import lt.saltyjuice.dragas.hw.vinted.discount.providing.ProposedDiscountInvoice;

/**
 * Tests for "La Poste Large shipment", which returns the {@link LaposteLargeInvoiceConfirmingStage}.
 * It is up to runtime to decide what is "La Poste" and what is "Large".
 * Domain specific rule. Should it be shipped with original module?
 *
 * @see LaposteLargeInvoiceConfirmingStage
 */
public class LaposteLargeInvoiceConfirmingRule extends DiscountRule {
    private final String LA_POSTE;
    private final String LARGE;
    private final int nth;
    private Stage stage;

    public LaposteLargeInvoiceConfirmingRule(String laPosteDefinition, String largeSizeDefinition, int nth) {
        this.LA_POSTE = laPosteDefinition;
        this.LARGE = largeSizeDefinition;
        this.nth = nth;
    }

    @Override
    public Stage getStage(Object input) throws InconsumableException {
        ProposedDiscountInvoice invoice = (ProposedDiscountInvoice) input;
        if (!(invoice.getShipper().equals(LA_POSTE) && invoice.getSize().equals(LARGE)))
            throw new InconsumableException(
                    String.format(
                            "Unsupported element. Expected %s %s, got %s %s",
                            LA_POSTE,
                            LARGE,
                            invoice.getShipper(),
                            invoice.getSize())
            );
        if (this.stage == null)
            this.stage = createStage();
        return this.stage;
    }

    @Override
    protected Stage createStage() {
        return new LaposteLargeInvoiceConfirmingStage(nth);
    }
}

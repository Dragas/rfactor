package lt.saltyjuice.dragas.hw.vinted.discount.confirming;

import lt.saltyjuice.dragas.hw.factory.exception.InconsumableException;
import lt.saltyjuice.dragas.hw.vinted.discount.providing.ProposedDiscountInvoice;

import java.util.TreeMap;

/**
 * Tracks la poste shipments. By definition, the third shipment of each
 * month can only be fully discounted, otherwise the discount is cancelled.
 * But this implementation provides ability to configure that via constructor.
 * This class is mutating: it doesn't create new objects for provided input,
 * but instead returns same element.
 *
 * <p>
 * The class uses regular {@link TreeMap} for counter
 * tracking. As a result, this isn't usable in concurrent environment.
 * Plus, since regular integers are used, this class will produce
 * incorrect results every (1 << 32) elements beyond the first 3
 * for the same YYYY-MM combo due to how integer overflows work in Java.
 * I do not expect tests for such large amounts of data, thus
 * this class does not take into account integer overflows.
 * </p>
 *
 * @see MonthlyStage
 */
public class LaposteLargeInvoiceConfirmingStage extends MonthlyStage {

    private final int firstNthShipment;

    public LaposteLargeInvoiceConfirmingStage(int firstNthShipment) {
        this.firstNthShipment = firstNthShipment;
    }

    @Override
    public Object apply(Object input) throws InconsumableException {
        ProposedDiscountInvoice invoice = (ProposedDiscountInvoice) input;
        String date = getYearMonthCombo(invoice.getDate());
        int count = monthlyTrackingMap.computeIfAbsent(date, (key) -> 0) + 1;
        if (count != firstNthShipment)
            invoice.setDiscount(0);
        monthlyTrackingMap.put(date, count);
        return invoice;
    }
}

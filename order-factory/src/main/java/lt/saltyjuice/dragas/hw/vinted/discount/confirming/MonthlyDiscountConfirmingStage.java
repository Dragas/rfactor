package lt.saltyjuice.dragas.hw.vinted.discount.confirming;

import lt.saltyjuice.dragas.hw.factory.exception.InconsumableException;
import lt.saltyjuice.dragas.hw.vinted.discount.providing.ProposedDiscountInvoice;

/**
 * Confirms discounts for each provided invoice according to
 * the monthly limit.
 * <p>
 * Discounts are confirmed in the following order:
 * <ol>
 * <li>if requested credit is greater than remainder credit, only remainder credit is applied</li>
 * <li>if requested credit is greater than price, only price is applied as requested credit</li>
 * <li>if requested credit is less than 0, no credit is applied</li>
 * </ol>
 * <p>
 * Due to the prior definition, this class modifies provided {@link ProposedDiscountInvoice} object
 * and returns the same one. This stage should be last one in {@link ConfirmingDiscountFactory}
 */
public class MonthlyDiscountConfirmingStage extends MonthlyStage {
    private final int limit;

    public MonthlyDiscountConfirmingStage(int limit) {
        this.limit = limit;
    }


    @Override
    public Object apply(Object input) throws InconsumableException {
        ProposedDiscountInvoice invoice = (ProposedDiscountInvoice) input;
        String date = getYearMonthCombo(invoice.getDate());
        int usedCreditForThatMonth = monthlyTrackingMap.computeIfAbsent(date, (key) -> 0);
        int requestedDiscount = invoice.getDiscount();
        int price = invoice.getPrice();
        // if requested discount is greater than
        // already consumed credit,
        // apply the remaining credit
        if (usedCreditForThatMonth + requestedDiscount > limit) {
            requestedDiscount = limit - usedCreditForThatMonth;
        }
        // if requested discount is greater than the requested price,
        // apply as much as necessary
        if (requestedDiscount > price) {
            requestedDiscount = price;
        }
        // if requested discount is negative, reduce it to zero
        if (requestedDiscount < 0) {
            requestedDiscount = 0;
        }
        invoice.setDiscount(requestedDiscount);
        invoice.setPrice(price - requestedDiscount);
        usedCreditForThatMonth += requestedDiscount;
        monthlyTrackingMap.put(date, usedCreditForThatMonth);
        return invoice;
    }
}

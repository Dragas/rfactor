package lt.saltyjuice.dragas.hw.vinted.discount.providing;

import lt.saltyjuice.dragas.hw.factory.AbstractFactory;
import lt.saltyjuice.dragas.hw.factory.Rule;
import lt.saltyjuice.dragas.hw.factory.exception.InconsumableException;

/**
 * Generates {@link ProposedDiscountInvoice} from {@link lt.saltyjuice.dragas.hw.vinted.invoice.Invoice}.
 * <p>
 * Applies rules in "first matches" fashion.
 */
public class ProposedDiscountFactory extends AbstractFactory {
    @Override
    public Object apply(Object input) throws InconsumableException {
        for (Rule rule : rules) {
            try {
                return rule.getStage(input).apply(input);
            } catch (InconsumableException e) {

            }
        }
        throw new InconsumableException("Unable to consume item");
    }
}

package lt.saltyjuice.dragas.hw.vinted.validation;

import lt.saltyjuice.dragas.hw.factory.exception.InconsumableException;
import lt.saltyjuice.dragas.hw.vinted.order.Order;

import java.util.Calendar;

/**
 * Date validation rule.
 * <p>
 * According to definition in {@link lt.saltyjuice.dragas.hw.vinted.discount.confirming.MonthlyStage}
 * this class attempts to validate input for YYYY-MM-DD format.
 * <p>
 * This class is not mutating,
 */
public class DateValidatingRule extends ValidatingRule {
    @Override
    protected void validate(Object input) throws InconsumableException {
        Order it = (Order) input;
        String date = it.getDate();
        if (date.length() != 10)
            throwInvalidDateException(date);
        String[] parts = date.split("-");
        if (parts.length != 3)
            throwInvalidDateException(date);
        Calendar calendar = new Calendar.Builder().setCalendarType("iso8601").build(); // must use iso8601 every time
        calendar.setLenient(false);
        calendar.set(Calendar.YEAR, Integer.parseInt(parts[0]));
        calendar.set(Calendar.MONTH, Integer.parseInt(parts[1]) - 1); // months are zero based [shrugging emoji]
        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(parts[2]));
        try {
            calendar.getTime();
        } catch (IllegalArgumentException e) {
            throwInvalidDateException(date, e);
        }
    }

    private void throwInvalidDateException(String date) throws InconsumableException {
        throwInvalidDateException(date, null);
    }

    private void throwInvalidDateException(String date, Exception e) throws InconsumableException {
        throw new InconsumableException(String.format("Unexpected Date string! Expected format YYYY-MM-DD, got %s", date), e);
    }
}

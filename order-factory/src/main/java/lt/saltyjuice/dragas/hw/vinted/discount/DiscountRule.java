package lt.saltyjuice.dragas.hw.vinted.discount;

import lt.saltyjuice.dragas.hw.factory.Rule;
import lt.saltyjuice.dragas.hw.factory.Stage;
import lt.saltyjuice.dragas.hw.factory.exception.InconsumableException;

/**
 * Abstraction to encourage rules to use "common" api to create stages.
 */
public abstract class DiscountRule implements Rule {
    @Override
    public Stage getStage(Object input) throws InconsumableException {
        return createStage();
    }

    protected abstract Stage createStage();
}

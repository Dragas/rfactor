package lt.saltyjuice.dragas.hw.vinted.validation;

import lt.saltyjuice.dragas.hw.factory.EmptyStage;
import lt.saltyjuice.dragas.hw.factory.Rule;
import lt.saltyjuice.dragas.hw.factory.Stage;
import lt.saltyjuice.dragas.hw.factory.exception.InconsumableException;

/**
 * Basic validating rule. Rules that are intended for validation
 * should extend this one instead, as they are not meant to return
 * some stage. By default, this returns {@link EmptyStage#DEFAULT}
 */
public abstract class ValidatingRule implements Rule {
    @Override
    public Stage getStage(Object input) throws InconsumableException {
        validate(input);
        return EmptyStage.DEFAULT;
    }

    protected abstract void validate(Object input) throws InconsumableException;
}

package lt.saltyjuice.dragas.hw.vinted.discount.confirming;

import lt.saltyjuice.dragas.hw.factory.Stage;

import java.util.Map;
import java.util.TreeMap;

/**
 * <p>
 * There are several ways you can extract year month
 * combo from string date. It is expected that somewhere
 * earlier in the chain the date formatting has passed
 * some validation and the date field indeed contains
 * a valid ISO8601 date which is one of:
 * <ul>
 * <li>YYYY-MM-DD</li>
 * <li>YYYYMMDD</li>
 * <li>YYYY-MM</li>
 * <li>--MM-DD</li>
 * <li>--MMDD</li>
 * </ul>
 * but not YYYYMM.
 * </p>
 * <p>
 * The format also supports expansion for years, but
 * that has to be agreed on prior. Since I didn't
 * ask enough questions, I'll just claim that YYYY-MM-DD is
 * the only valid format.
 * </p>
 * This abstract stage intends to "standartize" how month dependendant stages work.
 *
 * <p>
 * Due to prior assumptions, this stage enforces a "first 7 characters
 * of the date string" standard. It is up to implementation how the internal
 * counter is compared.
 * </p>
 *
 * <p>
 * To compensate for possible performance issues, this class enforces a
 * {@link TreeMap} data structure, which uses red-black tree implementation
 * for storage. This ensures that the data structure is sorted by keys
 * via natural order.
 * </p>
 */
public abstract class MonthlyStage implements Stage {
    protected final Map<String, Integer> monthlyTrackingMap = new TreeMap<>();


    protected final String getYearMonthCombo(String input) {
        return input.substring(0, 7);
    }
}

package lt.saltyjuice.dragas.hw.vinted.discount.confirming;

import lt.saltyjuice.dragas.hw.factory.AnyMatchFactory;
import lt.saltyjuice.dragas.hw.factory.exception.InconsumableException;
import lt.saltyjuice.dragas.hw.vinted.discount.DiscountedInvoice;
import lt.saltyjuice.dragas.hw.vinted.discount.providing.ProposedDiscountInvoice;

/**
 * An Any match factory implementation that generates an invoice from
 * provided proposed invoice
 */
public class ConfirmingDiscountFactory extends AnyMatchFactory {
    @Override
    public Object apply(Object input) throws InconsumableException {
        ProposedDiscountInvoice proposedInvoice = (ProposedDiscountInvoice) super.apply(input);
        DiscountedInvoice invoice = new DiscountedInvoice(proposedInvoice);
        return invoice;
    }
}

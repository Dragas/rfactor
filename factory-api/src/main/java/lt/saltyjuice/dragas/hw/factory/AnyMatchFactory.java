package lt.saltyjuice.dragas.hw.factory;

import lt.saltyjuice.dragas.hw.factory.exception.InconsumableException;

/**
 * Any match factory is the least strict factory
 * of them all. It applies all rules on incoming objects
 * and does not care whether or not they throw.
 * <p>
 * Useful when you want to modify the input.
 */
public class AnyMatchFactory extends AbstractFactory {
    @Override
    public Object apply(Object input) throws InconsumableException {
        for (Rule rule : rules) {
            try {
                rule.getStage(input).apply(input);
            } catch (InconsumableException e) {

            }
        }
        return input;
    }
}

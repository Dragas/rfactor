package lt.saltyjuice.dragas.hw.factory.exception;

import lt.saltyjuice.dragas.hw.factory.Factory;

/**
 * Implementations of {@link Factory Factory} are encouraged
 * to throw this exception when theyre provided an object they cannot consume
 */
public class InconsumableException extends Exception {
    public InconsumableException() {
        super();
    }

    public InconsumableException(String message) {
        super(message);
    }

    public InconsumableException(String message, Throwable cause) {
        super(message, cause);
    }

    public InconsumableException(Throwable cause) {
        super(cause);
    }

    protected InconsumableException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

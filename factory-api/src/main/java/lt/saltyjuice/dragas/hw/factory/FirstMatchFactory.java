package lt.saltyjuice.dragas.hw.factory;

import lt.saltyjuice.dragas.hw.factory.exception.InconsumableException;

/**
 * First match factory is such factory that tests all
 * internal rules against input object and returns
 * on first matching rule.
 * <p>
 * In case that there is no matching rule, the factory throws.
 */
public class FirstMatchFactory extends AbstractFactory {
    @Override
    public Object apply(Object input) throws InconsumableException {
        for (Rule rule : rules) {
            try {
                return rule.getStage(input).apply(input);
            } catch (InconsumableException e) {

            }
        }
        throw new InconsumableException("Unsupported element");
    }
}

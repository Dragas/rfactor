package lt.saltyjuice.dragas.hw.factory;

import lt.saltyjuice.dragas.hw.factory.exception.InconsumableException;

/**
 * Basic factory, which is supposed to invoke provided rules
 * on incoming instances of I to produce instances of Stages, which when invoked produce an O.
 * <br>
 * <br>
 * Factories themselves are intended to test the incoming objects against installed
 * {@link Rule} implementations.
 * <p>
 * Rules are evaluated in "logical or" fashion. If all of them
 * fails evaluating, the incoming object from Provider is considered to be unusable
 * and thus must throw {@link InconsumableException}. It is left up to
 * implementations to choose whether or not should they return
 * an "invalid" result for unusable object.
 *
 * Since Factory implements Rule interface, you may opt to group multiple rule layers under them
 * in case a complex singular stage choosing mechanism is necessary.
 *
 * Since Factory implements Stage interface, in conjunction with Rule interface, to return
 * the factory itself which would perform operations necessary for that particular factory.
 */
public interface Factory extends Rule, Stage {

    /**
     * Appends rule to current engine which is tested against incoming objects.
     * Implementations are encouraged to return the same Factory to provide builder-like
     * construction pattern.
     *
     * @param rule rules that are tested against to return {@link Stage} for provided input object
     * @return the current factory
     */
    Factory addRule(Rule rule);

    /**
     * Returns a resulting object from provided input
     *
     * @param input the I object that is supposed to be consumed
     * @return O object that is produced by this factory
     * @throws InconsumableException when there are no rules to generate pipeline for the provided argument
     * @deprecated since factories implement Stages, you should be using apply instead.
     */
    default Object run(Object input) throws InconsumableException {
        return apply(input);
    }
}

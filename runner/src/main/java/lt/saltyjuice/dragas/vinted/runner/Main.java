package lt.saltyjuice.dragas.vinted.runner;

import lt.saltyjuice.dragas.hw.factory.ChainingFactory;
import lt.saltyjuice.dragas.hw.factory.EmptyRule;
import lt.saltyjuice.dragas.hw.factory.Factory;
import lt.saltyjuice.dragas.hw.factory.FirstMatchFactory;
import lt.saltyjuice.dragas.hw.factory.exception.InconsumableException;
import lt.saltyjuice.dragas.hw.vinted.discount.confirming.ConfirmingDiscountFactory;
import lt.saltyjuice.dragas.hw.vinted.discount.confirming.LaposteLargeInvoiceConfirmingRule;
import lt.saltyjuice.dragas.hw.vinted.discount.confirming.MonthlyDiscountConfirmingRule;
import lt.saltyjuice.dragas.hw.vinted.discount.providing.PercentageDiscountRule;
import lt.saltyjuice.dragas.hw.vinted.invoice.ShipmentServiceFactory;
import lt.saltyjuice.dragas.hw.vinted.invoice.ShipmentSizeRule;
import lt.saltyjuice.dragas.hw.vinted.order.Order;
import lt.saltyjuice.dragas.hw.vinted.order.OrderProvider;
import lt.saltyjuice.dragas.hw.vinted.string.StringifyingStage;
import lt.saltyjuice.dragas.hw.vinted.validation.DateValidatingRule;
import lt.saltyjuice.dragas.hw.vinted.validation.ServiceValidatingRule;
import lt.saltyjuice.dragas.hw.vinted.validation.SizeValidatingRule;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Iterator;

public class Main {

    private static final String MONDIAL_RELAY = "MR";
    private static final String LA_POSTE = "LP";
    private static final String SMALL = "S";
    private static final String MEDIUM = "M";
    private static final String LARGE = "L";

    public static void main(String[] args) throws FileNotFoundException {
        FileInputStream fin = new FileInputStream("input.txt");
        Iterator<Order> orderProvider = new OrderProvider(fin); // should this be refactored into factory? [thinking emoji]

        Factory validatingFactory = new ChainingFactory() // throws on error
                .addRule(new DateValidatingRule())
                .addRule(new ServiceValidatingRule(LA_POSTE, MONDIAL_RELAY))
                .addRule(new SizeValidatingRule(SMALL, MEDIUM, LARGE));
        Factory laPosteShipmentFactory = new ShipmentServiceFactory(LA_POSTE) // creating
                .addRule(new ShipmentSizeRule(SMALL, 150))
                .addRule(new ShipmentSizeRule(MEDIUM, 490))
                .addRule(new ShipmentSizeRule(LARGE, 690));
        Factory mondialRelayShipmentFactory = new ShipmentServiceFactory(MONDIAL_RELAY) // creating
                .addRule(new ShipmentSizeRule(SMALL, 200))
                .addRule(new ShipmentSizeRule(MEDIUM, 300))
                .addRule(new ShipmentSizeRule(LARGE, 400));
        Factory invoiceFactory = new FirstMatchFactory() // creating
                .addRule(laPosteShipmentFactory)
                .addRule(mondialRelayShipmentFactory);
        Factory proposedDiscountFactory = new FirstMatchFactory() // mutating
                .addRule(new PercentageDiscountRule(100)
                        .expectedService(LA_POSTE)
                        .expectedSize(LARGE))
                .addRule(new PercentageDiscountRule(25)
                        .expectedSize(SMALL)
                        .expectedService(MONDIAL_RELAY))
                .addRule(new PercentageDiscountRule(0));
        Factory confirmedDiscountFactory = new ConfirmingDiscountFactory() // creating
                .addRule(new LaposteLargeInvoiceConfirmingRule(LA_POSTE, LARGE, 3))
                .addRule(new MonthlyDiscountConfirmingRule(1000));

        Factory main = new ChainingFactory()
                .addRule(validatingFactory)
                .addRule(invoiceFactory)
                .addRule(proposedDiscountFactory)
                .addRule(confirmedDiscountFactory)
                .addRule(new EmptyRule(
                        new StringifyingStage()
                ))
                /* string conversion factory */;

        // string conversion factory -> invoice
        while (orderProvider.hasNext()) {
            Order it = orderProvider.next();
            try {
                Object result = main.apply(it);
                System.out.println(result);
            } catch (InconsumableException e) {
                System.out.println(
                        String.format("%s %s %s Ignored",
                                it.getDate(),
                                it.getShipper(),
                                it.getSize())
                                .replaceAll("\\s+", " ")
                ); // looks like an argument for sprintf
            }
        }

    }
}
